﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MBTwitchAPI.Classes;

namespace APITester
{
    class Program
    {

        private static TwitchClient m_client;

        static void Main(string[] args)
        {
            Console.WriteLine("####################");
            Console.WriteLine("#    API-Tester    #");
            Console.WriteLine("####################");
            Console.WriteLine("\n\n\n\n\n");


            m_client = new TwitchClient("maddinbot", "XXX");

            m_client.OnChatMessage += Client_OnChatMessage;
            m_client.OnJoinedChannel += Client_OnJoinedChannel;
            m_client.OnRawMessage += Client_OnRawMessage;

            m_client.Connect();
            m_client.JoinChannel("maddinbot");

            Console.ReadKey();
        }

        private static void Client_OnRawMessage(string message)
        {
            //Console.WriteLine(message);
        }

        private static void Client_OnJoinedChannel(string channel)
        {
            m_client.SendMessage(channel, "Dieser Channel steht nun unter meiner Leitung!");
        }

        private static void Client_OnChatMessage(MBTwitchAPI.Classes.Message.ChatMessage message)
        {
            Console.WriteLine($"[{message.Channel}] <{message.Sender}>: {message.Message}");

            var msgSplit = message.Message.Split(' ');


            // Basic Command-Testing
            var commandIdentifier = msgSplit[0].ToLower();

            switch(commandIdentifier)
            {
                case "!join":
                    if (msgSplit[1] != string.Empty)
                    {
                        m_client.JoinChannel(msgSplit[1]);
                    }
                    break;

                case "!part":
                    if (msgSplit[1] != string.Empty)
                    {
                        m_client.PartChannel(msgSplit[1]);
                    }
                    break;

                default:
                    // nothing
                    break;
            }
        }
    }
}