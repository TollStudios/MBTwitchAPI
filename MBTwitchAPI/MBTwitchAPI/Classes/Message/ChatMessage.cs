﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MBTwitchAPI.Classes.Message
{
    /// <summary>
    /// Indicates a "normal" ChatMessage with some interesting data
    /// </summary>
    public class ChatMessage
    {
        /// <summary>
        /// Represents the Channel in which the Message was sent
        /// </summary>
        public string Channel { get; private set; }

        /// <summary>
        /// Represents the User that has sent the Message
        /// </summary>
        public string Sender { get; private set; } // TODO Create class for the User (Follower Status and all that good stuff ...)

        /// <summary>
        /// The send Message
        /// </summary>
        public string Message { get; private set; }

        /// <summary>
        /// If the Message was sent from the Bot itself
        /// </summary>
        public bool Self { get; private set; }

        /// <summary>
        /// Create a new instance of the ChatMessage Class with the needed information
        /// </summary>
        /// <param name="channel">The Channel in which the Message was sent</param>
        /// <param name="sender">The User that has sent the Message</param>
        /// <param name="message">The Message that was sent</param>
        public ChatMessage(string channel, string sender, string message, bool self)
        {
            this.Channel    = channel;
            this.Sender     = sender;
            this.Message    = message;
            this.Self       = self;
        }
    }
}