﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace MBTwitchAPI.Classes.Message
{
    internal class MessageParser
    {
        #region Constants

        /// <summary>
        /// The Regex-String for the User-Name of the Sender
        /// </summary>
        private const string mc_regex_userName = @":(\w*)!";

        /// <summary>
        /// The Regex-String for the Channel-Name and the actual Message
        /// </summary>
        private const string mc_regex_channelAndMessage = @"#(\w*) :(.*)";

        #endregion

        /// <summary>
        /// Parses a normal Chat-Message into a class
        /// </summary>
        /// <param name="rawmsg">The raw Message from the Twitch-IRC-API</param>
        /// <param name="clientUserName">The name of the Clients User to check if the message came from the Client itself</param>
        /// <returns>Returns the Parsed Message as ChatMessage Class or Null if something went wrong</returns>
        internal static ChatMessage ParseNormalChatMessage(string rawmsg, string clientUserName)
        {
            // if it's not a "normal" Chat Message, then return with null
            if (!rawmsg.Contains("PRIVMSG"))
            {
                return null;
            }

            string username = "";
            string channel = "";
            string message = "";

            var userNameMatch = Regex.Match(rawmsg, mc_regex_userName);
            var channelMessageMatch = Regex.Match(rawmsg, mc_regex_channelAndMessage);

            if (userNameMatch.Success && userNameMatch.Groups.Count >= 2)
            {
                username = userNameMatch.Groups[1].Value;
            }
            else
            {
                return null;
            }

            if (channelMessageMatch.Success && channelMessageMatch.Groups.Count >= 3)
            {
                channel = channelMessageMatch.Groups[1].Value;
                message = channelMessageMatch.Groups[2].Value;
            }

            // if the Message is from the Bot itself
            bool self = username == clientUserName;

            return new ChatMessage(channel, username, message, self);
        }
    }
}