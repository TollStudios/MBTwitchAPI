﻿using System;
using System.Net.Sockets;
using System.IO;
using System.Threading;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using MBTwitchAPI.Classes.Message;

namespace MBTwitchAPI.Classes
{
    // ##############################################################################################################
    // 
    // TwitchClient API to communicate and work with Twitch
    // Multiplatform
    // 
    // Version        : 0.0.1a (30.05.2018)
    // Author         : Martin Wagenführ (DatMaddin)
    // 
    // Last Change    : 30.05.2018 - DatMaddin - Refactoring, more Features and Implementing PartChannel()
    //  
    // ##############################################################################################################

    /// <summary>
    /// Client to communicate with Twitch
    /// </summary>
    public class TwitchClient
    {
        #region Constants

        /// <summary>
        /// The URL from the Twitch IRC-Server
        /// </summary>
        private const string    mc_twitchIrcUrl         = "irc.twitch.tv";

        /// <summary>
        /// The Port from the Twitch IRC-Server
        /// </summary>
        private const int       mc_twitchIrcPort        = 6667;

        /// <summary>
        /// The Indicator for a "normal" Chat-Message
        /// </summary>
        private const string    mc_normalMsgIndicator   = "PRIVMSG";


        #endregion Constants

        #region Public Members

        /// <summary>
        /// The "HeartBeat" of the TwitchClient
        /// = Represents how often the content of the NetworkStream is checked.
        /// Value is represented in Milliseconds
        /// Default: 100ms
        /// </summary>
        public int HeartBeat { get; set; } = 100;

        /// <summary>
        /// A list of all channels that this Client has joined
        /// </summary>
        public List<string> JoinedChannels { get; set; }

        /// <summary>
        /// Represents if the Client is already connected
        /// </summary>
        public bool Connected { get; private set; } = false;

        #endregion Public Members

        #region Private Members

        /// <summary>
        /// TCP Client to communicate with the IRC-Server from Twitch
        /// </summary>
        private TcpClient m_tcpClient;

        /// <summary>
        /// Reader to read the NetworkStream of the TCP-Client
        /// </summary>
        private StreamReader m_reader;

        /// <summary>
        /// Writer to write to the NetworkStream of the TCP-Client
        /// </summary>
        private StreamWriter m_writer;

        /// <summary>
        /// The name of the Client
        /// </summary>
        private string m_userName;

        /// <summary>
        /// The OAuth of the Client
        /// </summary>
        private string m_oauth;

        /// <summary>
        /// Time that controls the checking of the NetworkStream
        /// </summary>
        private Timer m_timer;

        #endregion Private Members

        #region Delegates and Events

        /// <summary>
        /// Event Handler for when a raw Message from the NetworkStream was sent.
        /// </summary>
        /// <param name="message">The RawMessage from the IRC</param>
        public delegate void RawMessageEventHandler(string message);

        /// <summary>
        /// The actual Event for the RawMessageEventHandler
        /// Gets fired, when a raw Message was caught on the NetworkStream from the Twitch IRC-Server.
        /// </summary>
        public event RawMessageEventHandler OnRawMessage;

        /// <summary>
        /// Event Handler for when a Chat Message was sent.
        /// </summary>
        /// <param name="message">The parsed Message Data in a ChatMessage Object</param>
        public delegate void ChatMessageEventHandler(ChatMessage message);

        /// <summary>
        /// The actual Event for the ChatMessageEventHandler
        /// Gets fired, when a Chat Message was caught on the NetworkStream. 
        /// The needed Data was parsed in a ChatMessage-Object
        /// </summary>
        public event ChatMessageEventHandler OnChatMessage;

        /// <summary>
        /// Event Handler for when the Client has connected to the Twitch IRC Server
        /// </summary>
        public delegate void ClientConnectedEventHandler();

        /// <summary>
        /// The actual Event for the ClientConnectedEventHandler 
        /// Gets fired, when the Client connected to the Twitch IRC Server
        /// </summary>
        public event ClientConnectedEventHandler OnClientConnected;

        /// <summary>
        /// Event Handler for when the Client has connected to a specified Channel
        /// </summary>
        /// <param name="channel"></param>
        public delegate void JoinedChannelEventHandler(string channel);

        /// <summary>
        /// The actual Event for the JoinedChannelEventHandler
        /// Gets fired, when the Client has connected to a specified Channel
        /// </summary>
        public event JoinedChannelEventHandler OnJoinedChannel;

        /// <summary>
        /// Event Handler for when the Client parts from a specified Channel
        /// </summary>
        /// <param name="channel"></param>
        public delegate void PartChannelEventHandler(string channel);

        /// <summary>
        /// The actual Event for the PartChannelEventHandler
        /// Gets fired, when the Client parts from a speciefied Channel
        /// </summary>
        public event PartChannelEventHandler OnPartChannel;


        #endregion Delegates and Events

        /// <summary>
        /// Creating an Instance for that TwitchClient without an initial Channel to join
        /// </summary>
        /// <param name="userName">The Username of the User the Client should operate on</param>
        /// <param name="clientOAuth">The OAuth Key for the Client - Generate One @ https://twitchapps.com/tmi/ </param>
        public TwitchClient (string userName, string clientOAuth)
        {
            m_userName = userName.ToLower();
            m_oauth = clientOAuth.ToLower();

            m_tcpClient = new TcpClient();

            this.JoinedChannels = new List<string>();
        }

        #region Public Methods

        /// <summary>
        /// Connects to the Twitch IRC-Server and starts the whole Process
        /// </summary>
        public void Connect ()
        {
            if (this.Connected)
            {
                return;
            }

            m_tcpClient.Client.Connect(mc_twitchIrcUrl, mc_twitchIrcPort);
            m_timer = new Timer(TimerTick, null, 0, this.HeartBeat);

            m_writer = new StreamWriter(m_tcpClient.GetStream());
            m_reader = new StreamReader(m_tcpClient.GetStream(), System.Text.Encoding.UTF8, true, 2560);

            // send stuff to the IRC to signalize that you want to connect
            m_writer.WriteLine("PASS {0}{1}NICK {2}{1}USER {2} 8 * :{2}", m_oauth, Environment.NewLine, m_userName);
            m_writer.Flush();

            this.Connected = true;

            OnClientConnected?.Invoke();
        }

        /// <summary>
        /// Sends a formatted Message to the IRC-Server so it will be displayed in the Twitch-Chat
        /// </summary>
        /// <param name="channel">The Channel where the message will be sent</param>
        /// <param name="message">The Message that will be sent</param>
        public void SendMessage(string channel, string message)
        {
            m_writer.WriteLine($"PRIVMSG #{channel} : {message}");
            m_writer.Flush();

            OnChatMessage?.Invoke(new ChatMessage(channel, m_userName, message, true));
        }

        /// <summary>
        /// Join a desired Channel
        /// </summary>
        /// <param name="channel">Channel to join</param>
        public void JoinChannel(string channel)
        {
            if (this.JoinedChannels.Contains(channel))
            {
                return;
            }

            channel = channel.ToLower();
            m_writer.WriteLine($"JOIN #{channel}");

            this.JoinedChannels.Add(channel);

            OnJoinedChannel?.Invoke(channel);

            m_writer.Flush();
        }

        /// <summary>
        /// Part the desired Channel
        /// </summary>
        /// <param name="channel">Channel to part</param>
        public void PartChannel(string channel)
        {
            if (!this.JoinedChannels.Contains(channel))
            {
                return;
            }

            channel = channel.ToLower();
            m_writer.WriteLine($"PART #{channel}");

            this.JoinedChannels.Remove(channel);

            OnPartChannel?.Invoke(channel);

            m_writer.Flush();
        }

        #endregion Public Methods

        #region Private Methods

        /// <summary>
        /// Every tick the Network Stream is getting processed to check if there are any new Messages
        /// </summary>
        /// <param name="state"></param>
        private void TimerTick (object state)
        {
            if ( m_reader != null )
            {
                if ( m_tcpClient.Available > 0 || m_reader.Peek() > 0 )
                {
                    string rawMessage = m_reader.ReadLine();
                    OnRawMessage?.Invoke(rawMessage);

                    ParseMessage(rawMessage);
                }
            }
        }

        /// <summary>
        /// Parses the raw message that was sent via IRC
        /// </summary>
        /// <param name="rawmsg">The raw Message from the IRC</param>
        private void ParseMessage(string rawmsg)
        {
            // "normal" Chat-Message
            if (rawmsg.Contains(mc_normalMsgIndicator))
            {
                var parsedMsg = MessageParser.ParseNormalChatMessage(rawmsg, m_userName);

                if (parsedMsg != null)
                {
                    OnChatMessage?.Invoke(parsedMsg);
                }
            }
        }

        #endregion Private Methods
    }
}